(define-module (example-package)
  #:use-module (pkill9 guix build-system love-wrapper)
  #:use-module (guix gexp) ;; local-file
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (gnu packages))

(define-public example-package
  (package
   (name "survive-ball")
   (version "0")
   (source (origin
            (method url-fetch)
            (uri "https://bananicorn.com/games/survive_ball/downloads/survive_ball.love")
            (sha256
             (base32
              "14akkq92grbq9kjkgccgiml48klxfv2qih85v4l5k4nic7l12822"))))
   (build-system love-wrapper-build-system)
   (arguments
    `(#:archived? #t))
   (home-page "")
   (synopsis "synopsis test")
   (description "")
   (license #f)))

(define-public not-tetris
  (package
   (name "not-tetris-2")
   (version "0")
   (source (origin
            (method url-fetch)
            (uri "http://stabyourself.net/dl.php?file=nottetris2/nottetris2-linux.zip")
            (sha256
             (base32
              "13585qcmds9yx8r1wnigd0mp6n3m5cx41sndn2xp84bn3rz77f72"))))
   (build-system love-wrapper-build-system)
   (arguments
    `(#:archived? #t
      #:love-package ,(@ (pkill9 packages love) love-0.7.2)))
   (home-page "")
   (synopsis "synopsis test")
   (description "")
   (license #f)))

not-tetris
