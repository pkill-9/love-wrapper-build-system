(define-module (pkill9 guix build-system love-wrapper)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix derivations)
  #:use-module (guix search-paths)
  #:use-module (guix build-system)
  #:use-module (guix build-system gnu)
  #:use-module (ice-9 match)
  #:export (%love-wrapper-build-system-modules
            love-wrapper-build
            love-wrapper-build-system))

;; Commentary:
;;
;; Standard build procedure for LÖVE wrapper packages in pkill9's channel.
;;
;; Code:

(define (default-love)
  "Return the default love package."
  ;; Lazily resolve the binding to avoid a circular dependency.
  (let ((module (resolve-interface '(gnu packages game-development))))
    (module-ref module 'love)))

(define (default-bash)
  "Return the default bash package."
  ;; Lazily resolve the binding to avoid a circular dependency.
  (let ((module (resolve-interface '(gnu packages bash))))
    (module-ref module 'bash)))

(define %love-wrapper-build-system-modules
  ;; Build-side modules imported by default.
  `((pkill9 guix build love-wrapper-build-system)
    ,@%gnu-build-system-modules))

(define* (lower name
                #:key source inputs native-inputs outputs system target
                (love-package (default-love))
                (bash (default-bash))
                #:allow-other-keys
                #:rest arguments)
  "Return a bag for NAME."
  (define private-keywords
    '(#:target #:inputs #:native-inputs #:love-package))

  (bag
    (name name)
    (system system)
    (host-inputs `(,@(if source
                         `(("source" ,source))
                         '())
                   ,@inputs
                   ,(list "tar" (module-ref (resolve-interface '(gnu packages base)) 'tar))
                   ,@(let ((compression (resolve-interface '(gnu packages compression))))
                       (map (match-lambda
                              ((name package)
                               (list name (module-ref compression package))))
                            `(("gzip" gzip)
                              ("bzip2" bzip2)
                              ("unzip" unzip)
                              ("xz" xz))))))
    (build-inputs `(("love" ,love-package)
                    ("bash" ,bash)
                    ,@native-inputs))
    (outputs outputs)
    (build love-wrapper-build)
    (arguments (strip-keyword-arguments private-keywords arguments))))

(define* (love-wrapper-build store name inputs
                     #:key source
                     (configure-flags ''())
                     (phases '(@ (pkill9 guix build love-wrapper-build-system)
                                 %standard-phases))
                     (outputs '("out"))
                     (archived? #f)
                     (search-paths '())
                     (system (%current-system))
                     (guile #f)
                     (imported-modules %love-wrapper-build-system-modules)
                     (modules '((pkill9 guix build love-wrapper-build-system)
                                (guix build utils))))
  "Build SOURCE with INPUTS."
  (define builder
    `(begin
       (use-modules ,@modules)
       (love-wrapper-build #:name ,name
                   #:source ,(match (assoc-ref inputs "source")
                               (((? derivation? source))
                                (derivation->output-path source))
                               ((source)
                                source)
                               (source
                                source))
                   #:system ,system
                   #:phases ,phases
                   #:outputs %outputs
                   #:archived? ,archived?
                   #:search-paths ',(map search-path-specification->sexp
                                         search-paths)
                   #:inputs %build-inputs)))

  (define guile-for-build
    (match guile
      ((? package?)
       (package-derivation store guile system #:graft? #f))
      (#f                                         ; the default
       (let* ((distro (resolve-interface '(gnu packages commencement)))
              (guile  (module-ref distro 'guile-final)))
         (package-derivation store guile system #:graft? #f)))))

  (build-expression->derivation store name builder
                                #:inputs inputs
                                #:system system
                                #:modules imported-modules
                                #:outputs outputs
                                #:guile-for-build guile-for-build))

(define love-wrapper-build-system
  (build-system
    (name 'love-wrapper)
    (description "The build system for LÖVE (the 2D games framework) wrapper packages.")
    (lower lower)))
