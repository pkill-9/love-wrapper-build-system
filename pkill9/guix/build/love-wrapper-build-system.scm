;;Todo: Check the love and bash inputs are not native-inputs, i.e. won't get colected by the garbage collector
;; Put gzip in PATH for tar

(define-module (pkill9 guix build love-wrapper-build-system)
  #:use-module ((guix build gnu-build-system) #:prefix gnu:)
  #:use-module (guix build utils)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-26)
  #:export (%standard-phases
            love-wrapper-build))

;; Commentary:
;;
;; Builder-side code of the build procedure for LÖVE wrapper packages.
;;
;; Code:

(define* (build #:key name source inputs outputs archived? #:allow-other-keys)
  "Copy SOURCE into the build directory."
  (let* ((out (assoc-ref outputs "out"))
         (unzip (string-append (assoc-ref inputs "unzip") "/bin/unzip"))
         (tar (string-append (assoc-ref inputs "tar") "/bin/tar"))
        (love-package (assoc-ref inputs "love"))
        (bash (assoc-ref inputs "bash"))
        (bin-name name)
        (love-files-dir "share/love-files")
        (love-relative-file-path (string-append love-files-dir "/"
                                       (string-append name ".love"))))
  (begin
    (mkdir "build")
    (chdir "build")
    (mkdir "bin")
    (mkdir-p love-files-dir)
    (mkdir-p "share/applications")
    ;; If "archived? #t" then extract to ../unpack and copy love file from there
    (if archived?
        (begin
          (mkdir "../unpack")
          (if (string-suffix? ".zip" source)
              (invoke unzip "-d" "../unpack" source)
              (invoke tar "xvf" source "-C" "../unpack"))
          (copy-file (first (find-files "../unpack"
                               (lambda (file stat)
                                 (string-suffix? ".love" file))))
                     love-relative-file-path))
        (copy-file source love-relative-file-path))
    (with-output-to-file (string-append "bin/" bin-name)
      (lambda _
        (format #t
                "#!~a/bin/bash~@
~@
~a/bin/love ~a~%" bash
love-package
(string-append out "/" love-relative-file-path))))
    (chmod (string-append "bin/" bin-name) #o755)
    (with-output-to-file (string-append "share/applications/" bin-name ".desktop")
      (lambda _
        (format #t
                "[Desktop Entry]~@
                     Name=~a~@
                     Comment=A game made with LOVE.~@
                     Exec=~a~@
                     TryExec=~a~@
                     Icon=~@
                     Categories=Game~@
                     Type=Application~%"
                name
                (string-append out "/" love-files-dir "/" bin-name)
                (string-append out "/" love-files-dir "/" bin-name))))
    #t)))

(define* (install #:key inputs outputs #:allow-other-keys)
  "Install the package contents."
  (let* ((out (assoc-ref outputs "out"))
         (source (getcwd)))
    (copy-recursively source out)
    #t))

(define %standard-phases
  ;; Standard build phases, as a list of symbol/procedure pairs.
  (let-syntax ((phases (syntax-rules ()
                         ((_ p ...) `((p . ,p) ...)))))
    (phases build install)))

(define* (love-wrapper-build #:key inputs (phases %standard-phases)
                      #:allow-other-keys #:rest args)
  "Build the given love-wrapper package, applying all of PHASES in order."
  (apply gnu:gnu-build #:inputs inputs #:phases phases args))
